#pragma once

#include <cassert>
#include <string>

#include "index_buffer.h"
#include "shader.h"
#include "vertex_array.h"

#define GLCall(x)                                                              \
  GLClearErrors();                                                             \
  x;                                                                           \
  assert(GLLogCall(#x, __FILE__, __LINE__))

void GLClearErrors();

bool GLLogCall(const std::string &function, const std::string &file, int line);

class Renderer {
public:
  void Clear() const;

  void Draw(const VertexArray &va, const IndexBuffer &ib, const Shader &shader);
  void Draw(const VertexArray &va, size_t vertex_count, const Shader &shader);
};