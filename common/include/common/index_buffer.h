#pragma once

class IndexBuffer {
public:
    IndexBuffer(const unsigned int *data, unsigned int count);

    ~IndexBuffer();

    void Bind() const;

    void Unbind() const;

    unsigned int GetCount() const;

private:
    unsigned int renderer_id_{0};
    unsigned int count_;
};