#pragma once

#include <vector>

#include <GL/glew.h>


struct VertexBufferElement {
    unsigned int type;
    unsigned int count;
    unsigned char normalized;

    static unsigned int GetSizeOfType(unsigned int type) {
        switch (type) {
            case GL_FLOAT:
                return sizeof(GLfloat);
            case GL_UNSIGNED_INT:
                return sizeof(GLuint);
            case GL_BYTE:
                return sizeof(GLbyte);
            default:
                std::abort();
        }
    }
};

class VertexBufferLayout {
public:
    VertexBufferLayout() = default;

    ~VertexBufferLayout() = default;

    template<typename T>
    void Push(unsigned int count) {
        static_assert(false);
    }

    template<>
    void Push<float>(unsigned int count) {
        elements_.emplace_back(GL_FLOAT, count, GL_FALSE);
        stride_ += count * VertexBufferElement::GetSizeOfType(GL_FLOAT);
    }

    template<>
    void Push<unsigned int>(unsigned int count) {
        elements_.emplace_back(GL_UNSIGNED_INT, count, GL_FALSE);
        stride_ += count * VertexBufferElement::GetSizeOfType(GL_UNSIGNED_INT);
    }

    template<>
    void Push<unsigned char>(unsigned int count) {
        elements_.emplace_back(GL_UNSIGNED_BYTE, count, GL_TRUE);
        stride_ += count * VertexBufferElement::GetSizeOfType(GL_UNSIGNED_BYTE);
    }

    const std::vector<VertexBufferElement> &GetElements() const;

    unsigned int GetStride() const;

private:
    std::vector<VertexBufferElement> elements_;
    unsigned int stride_{0};
};