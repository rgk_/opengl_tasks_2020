#pragma once

#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

struct ShaderProgramSource {
    std::string vertex_source;
    std::string fragment_source;
};

class Shader {
public:
    explicit Shader(std::string filepath);

    ~Shader();

    void Bind() const;

    void Unbind() const;

    // Set uniforms
    void SetUniform1i(const std::string& name, int value);
    void SetUniform1f(const std::string& name, float value);
    void SetUniform4f(const std::string& name, float v0, float v1,float v2,float v3);
    void SetUniformMat4f(const std::string& name, const glm::mat4& matrix);

private:
    ShaderProgramSource ParseShader();
    unsigned int CompileShader(unsigned int type, const std::string &source);
    unsigned int CreateShader(const std::string &vertex_shader, const std::string &fragment_shader);
    int GetUniformLocation(const std::string& name);

private:
    unsigned int renderer_id_{0};
    std::string filepath_;
    std::unordered_map<std::string, int> uniform_location_cache_;
};