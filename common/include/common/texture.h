#pragma once

#include <string>

class Texture {
public:
    Texture(std::string filepath);

    ~Texture();

    void Bind(unsigned int slot = 0) const;

    void Unbind() const;

    int GetWidth() const;

    int GetHeight() const;

private:
    unsigned int renderer_id_;
    std::string filepath_;
    unsigned char *local_buffer_;
    int width_{0};
    int height_{0};
    int bpp_{0};
};