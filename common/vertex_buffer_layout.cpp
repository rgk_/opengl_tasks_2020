#include "vertex_buffer_layout.h"

const std::vector<VertexBufferElement> &VertexBufferLayout::GetElements() const {
    return elements_;
}

unsigned int VertexBufferLayout::GetStride() const {
    return stride_;
}
