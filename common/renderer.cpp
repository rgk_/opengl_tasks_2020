#include "renderer.h"

#include <iostream>

#include <GL/glew.h>

void GLClearErrors() {
    while (glGetError() != GL_NO_ERROR) {
    }
}

bool GLLogCall(const std::string &function, const std::string &file, int line) {
    while (auto error = glGetError()) {
        std::cerr << "[OpenGL Error] (" << error << ") " << function << " " << file << ":" << line << std::endl;
        return false;
    }

    return true;
}

void Renderer::Clear() const {
    GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray &va, const IndexBuffer &ib, const Shader &shader) {
    shader.Bind();
    va.Bind();
    ib.Bind();

    GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
}

void Renderer::Draw(const VertexArray &va, size_t vertex_count, const Shader &shader) {
  shader.Bind();
  va.Bind();

  GLCall(glDrawArrays(GL_TRIANGLES, 0, vertex_count));
}


