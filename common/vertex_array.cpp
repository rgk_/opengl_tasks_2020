#include "vertex_array.h"

#include "renderer.h"

VertexArray::VertexArray() {
    GLCall(glGenVertexArrays(1, &renderer_id_));
}

VertexArray::~VertexArray() {
    GLCall(glDeleteVertexArrays(1, &renderer_id_));
}

void VertexArray::AddBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout) {
    Bind();
    vb.Bind();

    unsigned int offset = 0;

    const auto &elements = layout.GetElements();
    for (size_t i = 0; i < elements.size(); ++i) {
        const auto &element = elements[i];

        GLCall(glEnableVertexAttribArray(i));
        GLCall(glVertexAttribPointer(i, element.count, element.type, element.normalized,
                                     layout.GetStride(), (void *) offset));

        offset += element.count * VertexBufferElement::GetSizeOfType(element.type);
    }
}

void VertexArray::Bind() const {
    GLCall(glBindVertexArray(renderer_id_));
}

void VertexArray::Unbind() const {
    GLCall(glBindVertexArray(0));
}

