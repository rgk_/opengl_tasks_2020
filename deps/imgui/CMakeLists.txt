add_library(imgui
        imgui.cpp
        imgui_demo.cpp
        imgui_draw.cpp
        imgui_tables.cpp
        imgui_widgets.cpp
        backends/imgui_impl_opengl3.cpp
        backends/imgui_impl_glfw.cpp
        )

target_include_directories(imgui PRIVATE
        include/imgui
        )


target_include_directories(imgui PUBLIC
        include
        )


target_link_libraries(imgui PUBLIC
        glfw
        glew
        glew_s
        )