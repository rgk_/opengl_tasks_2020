#pragma once

#include <vector>

#include <glm/glm.hpp>

struct Vertex {
  glm::vec3 pos;
  glm::vec3 normal;
};

std::vector<Vertex> CreateCube(double size);
std::vector<Vertex> CreateSphere(int n, double radius);
std::vector<Vertex> CreateMoebiusStrip(int n, double radius);
