#pragma once

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

extern glm::mat4 view_matrix;
extern glm::mat4 projection_matrix;


void CameraUpdate(GLFWwindow *window, double dt);