#include <array>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <stb/stb_image.h>

#include <common/renderer.h>

#include "camera.h"
#include "figures.h"

glm::mat4 GetViewMatrix() {
  return view_matrix;
}
glm::mat4 GetProjectionMatrix() {
  return projection_matrix;
}

const int kMaxN = 1000;
const int kMinN = 4;
int n = 10;

void PrintInfo() {
  const GLubyte* renderer = glGetString(GL_RENDERER);
  const GLubyte* version = glGetString(GL_VERSION);
  const GLubyte* glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);

  std::cerr << "Renderer: " << renderer << std::endl;
  std::cerr << "OpenGL context version: " << version << std::endl;
  std::cerr << "GLSL version: " << glslversion << std::endl;
}

extern float umin;
extern float umax;
extern float vmin;
extern float vmax;

int main() {
  glewExperimental = GL_TRUE;
  if (glfwInit() == 0) {
    std::cerr << "ERROR: could not start GLFW3\n";
    exit(1);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window =
      glfwCreateWindow(960, 540, "Hello World", nullptr, nullptr);
  if (window == nullptr) {
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

  if (glewInit() != GLEW_OK) {
    GLLogCall("glewInit()", __FILE__, __LINE__);
    std::cerr << "ERROR: could not start GLEW\n";
    exit(1);
  }

  glEnable(GL_DEPTH_TEST);

  PrintInfo();

  {
    float radius = 0.3f;
    bool polygon_borders = false;

    VertexArray va;

    Shader shader("796PonasenkoData1/shader.shader");

    va.Unbind();
    shader.Unbind();

    Renderer renderer;

    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init();

    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0) {
      glfwPollEvents();

      // mode
      if (polygon_borders) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      }

      if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        if (n < kMaxN) {
          ++n;
        }
      }
      if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        if (n > kMinN) {
          --n;
        }
      }

      static bool p_pressed = false;
      if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        if (!p_pressed) {
          p_pressed = true;
        }
      } else if (glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE) {
        if (p_pressed) {
          polygon_borders = !polygon_borders;
          p_pressed = false;
        }
      }

      // data
      auto vertices = CreateMoebiusStrip(n, radius);
      va.Bind();
      VertexBuffer vb(vertices.data(), vertices.size() * sizeof(vertices[0]));
      VertexBufferLayout layout;
      layout.Push<float>(3);
      layout.Push<float>(3);
      va.AddBuffer(vb, layout);

      // frame init
      renderer.Clear();

      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplGlfw_NewFrame();
      ImGui::NewFrame();

      // mvp update
      static double last_time = glfwGetTime();
      double dt = glfwGetTime() - last_time;
      last_time = glfwGetTime();

      CameraUpdate(window, dt);

      glm::mat4 view = view_matrix;
      glm::mat4 proj = projection_matrix;
      glm::mat4 model = glm::mat4(1.0f);
      glm::mat4 mvp = proj * view * model;

      shader.Bind();
      shader.SetUniformMat4f("u_MVP", mvp);

      // draw
      renderer.Draw(va, vertices.size(), shader);

      // set UI
      if (ImGui::Begin("task1", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("Move: wasd\nCamera: hjkl\nOther: +-p");
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
        ImGui::SliderInt("N", &n, kMinN, kMaxN);
        ImGui::SliderFloat("radius", &radius, -10.0f, 10.0f);
        ImGui::SliderFloat("umin", &umin, -1.0f, 1.0f);
        ImGui::SliderFloat("umax", &umax, -1.0f, 1.0f);
        ImGui::SliderFloat("vmin", &vmin, 0.0f, 7.0f);
        ImGui::SliderFloat("vmax", &vmax, 0.0f, 20.0f);
        ImGui::Checkbox("Polygons borders", &polygon_borders);
        ImGui::End();
      }

      // draw UI
      ImGui::Render();
      ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

      // update window size
      int width, height;
      glfwGetFramebufferSize(window, &width, &height);
      glViewport(0, 0, width, height);

      glfwSwapBuffers(window);
    }
  }

  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}
