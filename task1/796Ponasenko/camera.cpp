#include "camera.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>

glm::mat4 view_matrix;
glm::mat4 projection_matrix;

glm::quat RotationBetweenVectors(glm::vec3 start, glm::vec3 dest) {
  start = normalize(start);
  dest = normalize(dest);

  float cos_theta = dot(start, dest);
  glm::vec3 rotation_axis;

  if (cos_theta < -1 + 0.001f) {
    // special case when vectors in opposite directions:
    // there is no "ideal" rotation axis
    // So guess one; any will do as long as it's perpendicular to start
    rotation_axis = cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
    if (glm::length2(rotation_axis) < 0.01) {
      // bad luck, they were parallel, try again!
      rotation_axis = cross(glm::vec3(1.0f, 0.0f, 0.0f), start);
    }

    rotation_axis = normalize(rotation_axis);
    return glm::angleAxis(glm::radians(180.0f), rotation_axis);
  }

  rotation_axis = cross(start, dest);

  float s = sqrt((1 + cos_theta) * 2);
  float invs = 1 / s;

  return glm::quat(s * 0.5f, rotation_axis.x * invs, rotation_axis.y * invs,
                   rotation_axis.z * invs);
}

void CameraUpdate(GLFWwindow* window, double dt) {
  float near = 0.1f;
  float far = 500.0f;
  float speed = 1.0f;

  static glm::vec3 direction = {0.0f, 0.0f, 1.0f};
  static glm::vec3 up = {0.0f, 1.0f, 0.0f};
  static glm::vec3 right = {1.0f, 0.0f, 0.0f};
  static glm::vec3 position = {0.0f, 0.0f, -1.0f};

  direction = glm::normalize(direction);
  up = glm::normalize(up);
  right = glm::normalize(right);

  glm::vec3 shift = {0.0f, 0.0f, 0.0f};
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    shift = right;
    shift *= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    shift = -right;
    shift *= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    shift = direction;
    shift *= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    shift = -direction;
    shift *= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
    auto rotation = glm::rotate(glm::mat4(1.0f), 0.05f, direction);
    up = rotation * glm::vec4(up, 0.0f);
    right = rotation * glm::vec4(right, 0.0f);
  }
  if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
    auto rotation = glm::rotate(glm::mat4(1.0f), -0.05f, direction);
    up = rotation * glm::vec4(up, 0.0f);
    right = rotation * glm::vec4(right, 0.0f);
  }

  glm::quat r = {1.0f, 0.0f, 0.0f, 0.0f};
  if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
    r = 0.15f * RotationBetweenVectors(direction, right);
  }
  if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
    r = 0.15f * RotationBetweenVectors(up, direction);
  }
  if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
    r = 0.15f * RotationBetweenVectors(direction, up);
  }
  if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
    r = 0.15f * RotationBetweenVectors(right, direction);
  }

  direction = r * direction;
  up = r * up;
  right = r * right;

  position += shift;
  view_matrix = glm::lookAt(position, position + direction, up);

  int width, height;
  glfwGetFramebufferSize(window, &width, &height);

  projection_matrix =
      glm::perspective(glm::radians(60.0f), (float)width / height, near, far);
}
