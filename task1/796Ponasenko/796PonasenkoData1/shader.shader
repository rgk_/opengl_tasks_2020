#shader vertex
#version 330 core

layout(location=0) in vec4 position;
layout(location=1) in vec3 normal;

uniform mat4 u_MVP;

out vec3 v_Normal;

void main() {
    v_Normal = normal;

    gl_Position = u_MVP * position;
}

#shader fragment
#version 330 core

layout(location=0) out vec4 color;

in vec3 v_Normal;

void main() {
    color = vec4(0.5 * v_Normal, 1.0) + 0.5;
}
