#include "figures.h"

#include <glm/gtc/matrix_transform.hpp>


std::vector<Vertex> CreateCube(double size) {
  std::vector<Vertex> vertices;

  vertices.push_back({glm::vec3(size, -size, size), glm::vec3(1.0, 0.0, 0.0)});
  vertices.push_back({glm::vec3(size, size, -size), glm::vec3(1.0, 0.0, 0.0)});
  vertices.push_back({glm::vec3(size, size, size), glm::vec3(1.0, 0.0, 0.0)});

  // front 2
  vertices.push_back({glm::vec3(size, -size, size), glm::vec3(1.0, 0.0, 0.0)});
  vertices.push_back({glm::vec3(size, -size, -size), glm::vec3(1.0, 0.0, 0.0)});
  vertices.push_back({glm::vec3(size, size, -size), glm::vec3(1.0, 0.0, 0.0)});

  // left 1
  vertices.push_back(
      {glm::vec3(-size, -size, size), glm::vec3(0.0, -1.0, 0.0)});
  vertices.push_back(
      {glm::vec3(size, -size, -size), glm::vec3(0.0, -1.0, 0.0)});
  vertices.push_back({glm::vec3(size, -size, size), glm::vec3(0.0, -1.0, 0.0)});

  // left 2
  vertices.push_back(
      {glm::vec3(-size, -size, size), glm::vec3(0.0, -1.0, 0.0)});
  vertices.push_back(
      {glm::vec3(-size, -size, -size), glm::vec3(0.0, -1.0, 0.0)});
  vertices.push_back(
      {glm::vec3(size, -size, -size), glm::vec3(0.0, -1.0, 0.0)});

  // top 1
  vertices.push_back({glm::vec3(-size, size, size), glm::vec3(0.0, 0.0, 1.0)});
  vertices.push_back({glm::vec3(size, -size, size), glm::vec3(0.0, 0.0, 1.0)});
  vertices.push_back({glm::vec3(size, size, size), glm::vec3(0.0, 0.0, 1.0)});

  // top 2
  vertices.push_back({glm::vec3(-size, size, size), glm::vec3(0.0, 0.0, 1.0)});
  vertices.push_back({glm::vec3(-size, -size, size), glm::vec3(0.0, 0.0, 1.0)});
  vertices.push_back({glm::vec3(size, -size, size), glm::vec3(0.0, 0.0, 1.0)});

  // back 1
  vertices.push_back(
      {glm::vec3(-size, -size, size), glm::vec3(-1.0, 0.0, 0.0)});
  vertices.push_back({glm::vec3(-size, size, size), glm::vec3(-1.0, 0.0, 0.0)});
  vertices.push_back(
      {glm::vec3(-size, size, -size), glm::vec3(-1.0, 0.0, 0.0)});

  // back 2
  vertices.push_back(
      {glm::vec3(-size, -size, size), glm::vec3(-1.0, 0.0, 0.0)});
  vertices.push_back(
      {glm::vec3(-size, size, -size), glm::vec3(-1.0, 0.0, 0.0)});
  vertices.push_back(
      {glm::vec3(-size, -size, -size), glm::vec3(-1.0, 0.0, 0.0)});

  // right 1
  vertices.push_back({glm::vec3(-size, size, size), glm::vec3(0.0, 1.0, 0.0)});
  vertices.push_back({glm::vec3(size, size, size), glm::vec3(0.0, 1.0, 0.0)});
  vertices.push_back({glm::vec3(size, size, -size), glm::vec3(0.0, 1.0, 0.0)});

  // right 2
  vertices.push_back({glm::vec3(-size, size, size), glm::vec3(0.0, 1.0, 0.0)});
  vertices.push_back({glm::vec3(+size, size, -size), glm::vec3(0.0, 1.0, 0.0)});
  vertices.push_back({glm::vec3(-size, size, -size), glm::vec3(0.0, 1.0, 0.0)});

  // bottom 1
  vertices.push_back(
      {glm::vec3(-size, size, -size), glm::vec3(0.0, 0.0, -1.0)});
  vertices.push_back({glm::vec3(size, size, -size), glm::vec3(0.0, 0.0, -1.0)});
  vertices.push_back(
      {glm::vec3(size, -size, -size), glm::vec3(0.0, 0.0, -1.0)});

  // bottom 2
  vertices.push_back(
      {glm::vec3(-size, size, -size), glm::vec3(0.0, 0.0, -1.0)});
  vertices.push_back(
      {glm::vec3(size, -size, -size), glm::vec3(0.0, 0.0, -1.0)});
  vertices.push_back(
      {glm::vec3(-size, -size, -size), glm::vec3(0.0, 0.0, -1.0)});

  return vertices;
}

glm::vec3 Sphere(float u, float v, float radius) {
  return glm::vec3(cos(u) * sin(v) * radius, sin(u) * sin(v) * radius,
                   cos(v) * radius);
}

std::vector<Vertex> CreateSphere(int n, double radius) {
  unsigned int m = n / 2;

  std::vector<Vertex> vertices;

  for (unsigned int i = 0; i < m; i++) {
    float theta = (float)glm::pi<float>() * i / m;
    float theta1 = (float)glm::pi<float>() * (i + 1) / m;

    for (unsigned int j = 0; j < n; j++) {
      float phi =
          2.0f * (float)glm::pi<float>() * j / n + (float)glm::pi<float>();
      float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / n +
                   (float)glm::pi<float>();

      auto pos = [radius](float u, float v) {
        return Sphere(u, v, radius);
      };
      auto norm = [](float u, float v) {
        return Sphere(u, v, 1);
      };

      vertices.push_back({pos(phi, theta), norm(phi, theta)});
      vertices.push_back({pos(phi1, theta), norm(phi1, theta)});
      vertices.push_back({pos(phi1, theta1), norm(phi1, theta1)});

      vertices.push_back({pos(phi, theta), norm(phi, theta)});
      vertices.push_back({pos(phi, theta1), norm(phi, theta1)});
      vertices.push_back({pos(phi1, theta1), norm(phi1, theta1)});
    }
  }

  return vertices;
}

float umin = -0.4f;
float umax = 0.4f;
float vmin = 0.0f;
float vmax = 2 * (float)glm::pi<float>();

glm::vec3 Moebius(float u, float v, float radius) {
  // x = aa * (cos(v) + u * cos(v / 2) * cos(v))
  //  y = aa * (sin(v) + u * cos(v / 2) * sin(v))
  //  z = aa * u * sin(v / 2)
  return glm::vec3((cos(v) + u * cos(v / 2) * cos(v)) * radius,
                   (sin(v) + u * cos(v / 2) * sin(v)) * radius,
                   u * sin(v / 2) * radius);
}

glm::vec3 Normal(const glm::vec3& p1, const glm::vec3& p2,
                 const glm::vec3& p3) {
  auto u = p2 - p1;
  auto v = p3 - p1;

  return glm::normalize(glm::vec3{u.y * v.z - u.z * v.y, u.z * v.x - u.x * v.z,
                                  u.x * v.y - u.y * v.x});
}

std::vector<Vertex> CreateMoebiusStrip(int n, double radius) {
  int m = std::max(n / 16, 1);
  std::vector<Vertex> vertices;

  auto pos = [radius](float u, float v) {
    return Moebius(u, v, radius);
  };

  for (unsigned int i = 0; i < n; i++) {
    float theta = vmin + (vmax - vmin) * i / n;
    float theta1 = vmin + (vmax - vmin) * (i + 1) / n;

    for (unsigned int j = 0; j < m; j++) {
      float phi = umin + (umax - umin) * j / m;
      float phi1 = umin + (umax - umin) * (j + 1) / m;

      auto p1 = pos(phi, theta);
      auto p2 = pos(phi1, theta);
      auto p3 = pos(phi1, theta1);
      auto p4 = pos(phi, theta1);

      auto normal1 = Normal(p1, p2, p3);
      auto normal2 = Normal(p1, p3, p4);

      vertices.push_back({p1, normal1});
      vertices.push_back({p2, normal1});
      vertices.push_back({p3, normal1});

      vertices.push_back({p1, normal2});
      vertices.push_back({p4, normal2});
      vertices.push_back({p3, normal2});
    }
  }

  return vertices;
}
