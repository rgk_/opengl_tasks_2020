cmake_minimum_required(VERSION 3.17)
project(opengl_tasks_2021)

set(CMAKE_CXX_STANDARD 20)

# libs
add_subdirectory(deps)
add_subdirectory(common)

# apps
add_subdirectory(task1)
